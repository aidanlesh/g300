using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEvent : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        // lazy duck typing
        CoinCollector otherCoinCollector = other.gameObject.GetComponent<CoinCollector>();
        if (otherCoinCollector) {
            otherCoinCollector.coins += 1;
        }
        Destroy(gameObject);
    }
}
