using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    [SerializeField] private Transform LeftEdgeStart;
    [SerializeField] private Transform LeftEdgeEnd;
    [SerializeField] private Transform RightEdgeStart;
    [SerializeField] private Transform RightEdgeEnd;
    private Rigidbody2D rb;
    private SpriteRenderer sr;
    private Animator animator;
    public float moveSpeed = 1;
    public bool facingLeft = false;
    [SerializeField] private bool onGround = true;

    public bool isDead = false;
    [SerializeField] public GameObject loot;
    private float deadTimer = 0f;
    public float deadTime = 1f;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
    }


    private void Update()
    {
        sr.flipX = !facingLeft;
    }

    void FixedUpdate()
    {
        if (isDead) {
            if (deadTimer > deadTime) {
                FinishDying();
            }
            deadTimer += Time.fixedDeltaTime;
            return;
        }
        Vector2 LeftStart = LeftEdgeStart.position;
        Vector2 LeftEnd = LeftEdgeEnd.position;
        Vector2 RightStart = RightEdgeStart.position;
        Vector2 RightEnd = RightEdgeEnd.position;
        onGround = Physics2D.Raycast(LeftStart, RightStart - LeftStart, (RightStart - LeftStart).magnitude);
        if (onGround) {
            bool groundLeft = Physics2D.Raycast(LeftStart, LeftEnd - LeftStart, (LeftEnd - LeftStart).magnitude);
            bool groundRight = Physics2D.Raycast(RightStart, RightEnd - RightStart, (RightEnd - RightStart).magnitude);
            Vector2 vel = rb.velocity;
            vel.x = moveSpeed * (facingLeft ? -1 : 1);
            rb.velocity = vel;
            if (facingLeft && !groundLeft) {facingLeft = false;}
            if (!facingLeft && !groundRight) {facingLeft = true;}
        }
    }

    public void Die()
    {
        isDead = true;
        animator.SetTrigger("Die");
        transform.localScale = new Vector3(1f, 0.8f, 1f);
    }

    private void FinishDying()
    {
        Destroy(gameObject);
        if (loot) {
            Instantiate(loot, transform.position, Quaternion.identity);
        }
    }
}
