using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    private Rigidbody2D rb;
    public Animator animator;
    public Transform groundCheck;
    public float speed;
    public float jumpForce;

    public Transform jumpCheckStart;
    public Transform jumpCheckEnd;
    private float timeSinceJump = float.MaxValue;
    public float jumpCooldown = 0.1f;

    public LayerMask layerMask;
    public bool onGround = false;
    
    private RaycastHit2D[] standingOn = new RaycastHit2D[4];

    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // animation code
        animator.SetFloat("speed", rb.velocity.magnitude);
        
        // movement code
        Vector2 move = new Vector2();

        move.x = Input.GetAxis("Horizontal") * speed;
        
        
        
        Vector3 jumpCheckVector = jumpCheckEnd.position - jumpCheckStart.position;

        // fill standingOn with all floor collisions, empty if none. set onGround if there is at least 1 floor
        Array.Clear(standingOn, 0, standingOn.Length);
        Physics2D.RaycastNonAlloc(jumpCheckStart.position, jumpCheckVector, standingOn, jumpCheckVector.magnitude, layerMask);
        onGround = standingOn[0].collider != null;
        
        // jump logic
        if (Input.GetAxis("Jump") > 0 && timeSinceJump > jumpCooldown) {
            if (onGround) {
                move.y = jumpForce;
                timeSinceJump = 0;
            }
        }

        // enemy stomp logic
        foreach (RaycastHit2D floor in standingOn) {
            // this is disgusting but whatever
            // perhaps I would do this with an event system instead
            if (floor.collider != null) {
                Enemy enemyComponent = floor.collider.gameObject.GetComponent<Enemy>();
                if (enemyComponent) {
                    enemyComponent.Die();
                }
            }
        }
        
        rb.AddForce(move);
        timeSinceJump += Time.fixedDeltaTime;
    }
}
