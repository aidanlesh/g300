using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CoinText : MonoBehaviour
{

    private TextMeshProUGUI text;
    [SerializeField] private string prefix;
    public CoinCollector coinCollector;
    [SerializeField] private string suffix;
    
    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        text.text = prefix + coinCollector.coins.ToString() + suffix;
    }
}
